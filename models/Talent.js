const mongoose = require("mongoose");

const talentSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "name is required."],
    },
    experience: {
      type: Number,
      required: [true, "year of experience is required."],
    },
    photo: {
      type: String,
      required: true,
    },
    description: { type: String },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  }
);

module.exports = mongoose.model("Talent", talentSchema);
