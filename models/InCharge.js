const mongoose = require("mongoose");

const inChargeSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "name is required."],
    },
    photo: {
      type: String,
      required: true,
    },
    description: { type: String },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  }
);

module.exports = mongoose.model("InCharge", inChargeSchema);
