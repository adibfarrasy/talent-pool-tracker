require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
}); // Config environment

const mongoose = require("mongoose");
mongoose
  .connect(process.env.MONGO_URI, {})
  .then(() => console.log("MongoDB Connected"))
  /* istanbul ignore next */
  .catch((err) => console.log(err));

exports.Company = require("./Company");
exports.InCharge = require("./InCharge");
exports.Talent = require("./Talent");
exports.Tracker = require("./Tracker");
