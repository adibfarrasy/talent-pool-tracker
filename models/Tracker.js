const mongoose = require("mongoose");

const trackerSchema = new mongoose.Schema(
  {
    talent: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, "talent is required."],
      ref: "Talent",
    },
    pic: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, "PIC is required."],
      ref: "InCharge",
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, "company is required."],
      ref: "Company",
    },
    status: {
      type: String,
      required: true,
      enum: [
        "review",
        "HR interview",
        "User interview",
        "offer",
        "accepted",
        "rejected",
      ],
      default: "review",
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  }
);

module.exports = mongoose.model("Tracker", trackerSchema);
