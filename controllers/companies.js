const { Company } = require("../models");
const fs = require("fs");

class CompanyCtrl {
  async getMultipleCompanies(req, res, next) {
    try {
      const pageSize = parseInt(req.query.limit) || 15;
      const currentPage = req.query.page || 1;

      const data = await Company.find()
        .skip(pageSize * (currentPage - 1))
        .limit(pageSize)
        .sort("-createdAt");

      res.status(200).json({ data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }

  async getDetailCompany(req, res, next) {
    try {
      const data = await Company.findById(req.params.id);

      res.status(200).json({ data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }

  async createCompany(req, res, next) {
    try {
      const data = await Company.create(req.body);

      res.status(201).json({ message: "Company created.", data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async updateCompany(req, res, next) {
    try {
      if (req.body.photo) {
        const oldData = await Company.findById(req.params.id);
        fs.unlinkSync(`.${oldData.photo}`);
      }

      const data = await Company.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
      });

      res.status(200).json({ message: "Company updated.", data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async deleteCompany(req, res, next) {
    try {
      /* istanbul ignore next */
      if (process.env.NODE_ENV !== "test") {
        const data = await Company.findByIdAndDelete(req.params.id);
        fs.unlinkSync(`.${data.photo}`);
      }

      res.status(200).json({ message: "Company deleted." });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
}

module.exports = new CompanyCtrl();
