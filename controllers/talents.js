const { Talent } = require("../models");
const fs = require("fs");

class TalentCtrl {
  async getMultipleTalents(req, res, next) {
    try {
      const pageSize = parseInt(req.query.limit) || 15;
      const currentPage = req.query.page || 1;

      const data = await Talent.find()
        .skip(pageSize * (currentPage - 1))
        .limit(pageSize)
        .sort("-createdAt");

      res.status(200).json({ data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async getDetailTalent(req, res, next) {
    try {
      const data = await Talent.findById(req.params.id);

      res.status(200).json({ data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async createTalent(req, res, next) {
    try {
      const data = await Talent.create(req.body);

      res.status(201).json({ message: "Talent created.", data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async updateTalent(req, res, next) {
    try {
      if (req.body.photo) {
        const oldData = await Talent.findById(req.params.id);
        fs.unlinkSync(`.${oldData.photo}`);
      }

      const data = await Talent.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
      });

      res.status(200).json({ message: "Talent updated.", data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async deleteTalent(req, res, next) {
    try {
      /* istanbul ignore next */
      if (process.env.NODE_ENV !== "test") {
        const data = await Talent.findByIdAndDelete(req.params.id);
        fs.unlinkSync(`.${data.photo}`);
      }

      res.status(200).json({ message: "Talent deleted." });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
}

module.exports = new TalentCtrl();
