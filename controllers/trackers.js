const { Tracker } = require("../models");
const mongoose = require("mongoose");

class TrackerCtrl {
  async getMultipleTrackers(req, res, next) {
    try {
      const pageSize = parseInt(req.query.limit) || 15;
      const currentPage = req.query.page || 1;

      const queries = {};
      if (req.query.talent) queries.talent = req.query.talent;
      if (req.query.pic) queries.pic = req.query.pic;
      if (req.query.companies) queries.companies = req.query.companies;

      const data = await Tracker.find(queries)
        .skip(pageSize * (currentPage - 1))
        .limit(pageSize)
        .sort("-createdAt");

      res.status(200).json({ data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async getDetailTracker(req, res, next) {
    try {
      const data = await Tracker.findById(req.params.id);

      res.status(200).json({ data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async createTracker(req, res, next) {
    try {
      const data = await Tracker.create(req.body);

      res.status(201).json({ message: "Tracker created.", data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async updateTracker(req, res, next) {
    try {
      const data = await Tracker.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
      });

      res.status(200).json({ message: "Tracker updated.", data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
}

module.exports = new TrackerCtrl();
