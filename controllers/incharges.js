const { InCharge } = require("../models");
const fs = require("fs");

class InChargeCtrl {
  async getMultipleInCharges(req, res, next) {
    try {
      const pageSize = parseInt(req.query.limit) || 15;
      const currentPage = req.query.page || 1;

      const data = await InCharge.find()
        .skip(pageSize * (currentPage - 1))
        .limit(pageSize)
        .sort("-createdAt");

      res.status(200).json({ data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async getDetailIC(req, res, next) {
    try {
      const data = await InCharge.findById(req.params.id);

      res.status(200).json({ data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async createInCharge(req, res, next) {
    try {
      const data = await InCharge.create(req.body);

      res.status(201).json({ message: "PIC created.", data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async updateInCharge(req, res, next) {
    try {
      if (req.body.photo) {
        const oldData = await InCharge.findById(req.params.id);
        fs.unlinkSync(`.${oldData.photo}`);
      }

      const data = await InCharge.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
      });

      res.status(200).json({ message: "PIC updated.", data });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
  async deleteInCharge(req, res, next) {
    try {
      /* istanbul ignore next */
      if (process.env.NODE_ENV !== "test") {
        const data = await InCharge.findByIdAndDelete(req.params.id);
        fs.unlinkSync(`.${data.photo}`);
      }

      res.status(200).json({ message: "PIC deleted." });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }
}

module.exports = new InChargeCtrl();
