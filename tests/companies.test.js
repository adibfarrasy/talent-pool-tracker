const request = require("supertest");
const app = require("../app");
const { Company } = require("../models");

let companyId;

beforeAll(async () => {
  const companyTest = await Company.create({
    name: "Company Test",
    photo: "this/is/test-photo.png",
    description: "This is test description",
  });

  companyId = companyTest._id.toString();
});

describe("Pulse Check", () => {
  it("Success", async () => {
    const response = await request(app).get(`/`);

    expect(response.statusCode).toEqual(200);
  });
});

describe("Get Multiple Companies", () => {
  it("Success", async () => {
    const response = await request(app)
      .get(`/companies`)
      .query({ limit: 3, page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("data");
  });
});

describe("Get Detail Company", () => {
  it("Success", async () => {
    const response = await request(app).get(`/companies/${companyId}`);
    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
  });
  it("ID Not Valid", async () => {
    const response = await request(app).get(`/companies/${companyId}0`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Company Not Found", async () => {
    const response = await request(app).get(
      `/companies/${0 + companyId.slice(1)}`
    );
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Create Company", () => {
  it("Success", async () => {
    const response = await request(app).post(`/companies`).send({
      name: "Company Test 2",
      description: "We're hiring test",
    });
    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("data");
  });
  it("Missing Name", async () => {
    const response = await request(app).post(`/companies`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update Company", () => {
  it("Success", async () => {
    const response = await request(app).put(`/companies/${companyId}`).send({
      name: "Company Test Edited",
    });
    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("data");
  });
  it("ID Not Valid", async () => {
    const response = await request(app).put(`/companies/${companyId}0`).send({
      name: "Company Test Edited",
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Invalid Data", async () => {
    const response = await request(app).put(`/companies/${companyId}`).send({
      name: "",
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Company Not Found", async () => {
    const response = await request(app)
      .put(`/companies/${0 + companyId.slice(1)}`)
      .send({
        name: "Company Test Edited",
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Delete Company", () => {
  it("Success", async () => {
    const response = await request(app).delete(`/companies/${companyId}`);

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
  });
});
