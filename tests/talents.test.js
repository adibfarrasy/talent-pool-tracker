const request = require("supertest");
const app = require("../app");
const { Talent } = require("../models");

let talentId;

beforeAll(async () => {
  const talentTest = await Talent.create({
    name: "Talent Test",
    photo: "this/is/test-photo.png",
    experience: 1,
    description: "This is test description",
  });

  talentId = talentTest._id.toString();
});

describe("Pulse Check", () => {
  it("Success", async () => {
    const response = await request(app).get(`/`);

    expect(response.statusCode).toEqual(200);
  });
});

describe("Get Multiple Companies", () => {
  it("Success", async () => {
    const response = await request(app)
      .get(`/talents`)
      .query({ limit: 3, page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("data");
  });
});

describe("Get Detail Talent", () => {
  it("Success", async () => {
    const response = await request(app).get(`/talents/${talentId}`);
    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
  });
  it("ID Not Valid", async () => {
    const response = await request(app).get(`/talents/${talentId}0`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Talent Not Found", async () => {
    const response = await request(app).get(
      `/talents/${0 + talentId.slice(1)}`
    );
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Create Talent", () => {
  it("Success", async () => {
    const response = await request(app).post(`/talents`).send({
      name: "Talent Test 2",
      experience: 1,
      description: "Hello test",
    });
    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("data");
  });
  it("Missing Name", async () => {
    const response = await request(app).post(`/talents`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update Talent", () => {
  it("Success", async () => {
    const response = await request(app).put(`/talents/${talentId}`).send({
      name: "Talent Test Edited",
    });
    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("data");
  });
  it("ID Not Valid", async () => {
    const response = await request(app).put(`/talents/${talentId}0`).send({
      name: "Talent Test Edited",
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Invalid Data", async () => {
    const response = await request(app).put(`/talents/${talentId}`).send({
      name: "",
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Talent Not Found", async () => {
    const response = await request(app)
      .put(`/talents/${0 + talentId.slice(1)}`)
      .send({
        name: "Talent Test Edited",
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Delete Talent", () => {
  it("Success", async () => {
    const response = await request(app).delete(`/talents/${talentId}`);

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
  });
});
