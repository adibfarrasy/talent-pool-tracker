const request = require("supertest");
const app = require("../app");
const mongoose = require("mongoose");
const { Company, InCharge, Talent, Tracker } = require("../models");

let trackerId;
let companyId;
let picId;
let talentId;

beforeAll(async () => {
  const companyTest2 = await Company.create({
    name: "Company Test 2",
    photo: "this/is/test-photo.png",
    description: "This is test description",
  });

  const picTest2 = await InCharge.create({
    name: "InCharge Test 2",
    photo: "this/is/test-photo.png",
    description: "This is test description",
  });

  const talentTest2 = await Talent.create({
    name: "Talent Test 2",
    photo: "this/is/test-photo.png",
    experience: 1,
    description: "This is test description",
  });

  companyId = companyTest2._id;
  picId = picTest2._id;
  talentId = talentTest2._id;

  const trackerTest = await Tracker.create({
    name: "Tracker Test",
    talent: talentId,
    pic: picId,
    company: companyId,
  });

  trackerId = trackerTest._id.toString();
});

describe("Fail Pulse Check", () => {
  it("Success", async () => {
    const response = await request(app).get(`/break`);

    expect(response.statusCode).toEqual(404);
  });
});

describe("Get Multiple Trackers", () => {
  it("Success", async () => {
    const response = await request(app).get(`/trackers`).query({
      limit: 3,
      page: 1,
      talent: talentId,
      pic: picId,
      company: companyId,
    });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("data");
  });
});

describe("Get Detail Tracker", () => {
  it("Success", async () => {
    const response = await request(app).get(`/trackers/${trackerId}`);
    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
  });
  it("ID Not Valid", async () => {
    const response = await request(app).get(`/trackers/${trackerId}0`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Tracker Not Found", async () => {
    const response = await request(app).get(
      `/trackers/${0 + trackerId.slice(1)}`
    );
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Create Tracker", () => {
  it("Success", async () => {
    const response = await request(app).post(`/trackers`).send({
      name: "Tracker Test 2",
      talent: talentId,
      pic: picId,
      company: companyId,
    });
    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("data");
  });
  it("Missing Name", async () => {
    const response = await request(app).post(`/trackers`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update Tracker", () => {
  it("Success", async () => {
    const response = await request(app).put(`/trackers/${trackerId}`).send({
      name: "Tracker Test Edited",
    });
    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("data");
  });
  it("ID Not Valid", async () => {
    const response = await request(app).put(`/trackers/${trackerId}0`).send({
      name: "Tracker Test Edited",
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Invalid Data", async () => {
    const response = await request(app)
      .put(`/trackers/${trackerId}`)
      .send({ talent: "hello" });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Tracker Not Found", async () => {
    const response = await request(app)
      .put(`/trackers/${0 + trackerId.slice(1)}`)
      .send({
        name: "Tracker Test Edited",
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Talent ID Not Valid", async () => {
    const response = await request(app).put(`/trackers/${trackerId}`).send({
      talent: "hello",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Talent ID Not Found", async () => {
    const response = await request(app).put(`/trackers/${trackerId}`).send({
      talent: "615d25ccea2eed180579a1a9",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("PIC ID Not Valid", async () => {
    const response = await request(app).put(`/trackers/${trackerId}`).send({
      pic: "hello",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("PIC ID Not Found", async () => {
    const response = await request(app).put(`/trackers/${trackerId}`).send({
      pic: "615d25ccea2eed180579a1a9",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Company ID Not Valid", async () => {
    const response = await request(app).put(`/trackers/${trackerId}`).send({
      company: "hello",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Company ID Not Found", async () => {
    const response = await request(app).put(`/trackers/${trackerId}`).send({
      company: "615d25ccea2eed180579a1a9",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});
