const request = require("supertest");
const app = require("../app");
const { InCharge } = require("../models");

let picId;

beforeAll(async () => {
  const picTest = await InCharge.create({
    name: "InCharge Test",
    photo: "this/is/test-photo.png",
    description: "This is test description",
  });

  picId = picTest._id.toString();
});

describe("Pulse Check", () => {
  it("Success", async () => {
    const response = await request(app).get(`/`);

    expect(response.statusCode).toEqual(200);
  });
});

describe("Get Multiple Companies", () => {
  it("Success", async () => {
    const response = await request(app)
      .get(`/pics`)
      .query({ limit: 3, page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("data");
  });
});

describe("Get Detail InCharge", () => {
  it("Success", async () => {
    const response = await request(app).get(`/pics/${picId}`);
    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
  });
  it("ID Not Valid", async () => {
    const response = await request(app).get(`/pics/${picId}0`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("PIC Not Found", async () => {
    const response = await request(app).get(`/pics/${0 + picId.slice(1)}`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Create InCharge", () => {
  it("Success", async () => {
    const response = await request(app).post(`/pics`).send({
      name: "InCharge Test 2",
      description: "We're hiring test",
    });
    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("data");
  });
  it("Missing Name", async () => {
    const response = await request(app).post(`/pics`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update InCharge", () => {
  it("Success", async () => {
    const response = await request(app).put(`/pics/${picId}`).send({
      name: "InCharge Test Edited",
    });
    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("data");
  });
  it("ID Not Valid", async () => {
    const response = await request(app).put(`/pics/${picId}0`).send({
      name: "InCharge Test Edited",
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Invalid Data", async () => {
    const response = await request(app).put(`/pics/${picId}`).send({
      name: "",
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("InCharge Not Found", async () => {
    const response = await request(app)
      .put(`/pics/${0 + picId.slice(1)}`)
      .send({
        name: "InCharge Test Edited",
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Delete PIC", () => {
  it("Success", async () => {
    const response = await request(app).delete(`/pics/${picId}`);

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
  });
});
