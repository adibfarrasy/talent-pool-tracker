const express = require("express");

// Import validator
const {
  detailValidator,
  createValidator,
  updateValidator,
} = require("../middlewares/validators/talents");

// Import controller
const {
  getMultipleTalents,
  getDetailTalent,
  createTalent,
  updateTalent,
  deleteTalent,
} = require("../controllers/talents");

const { generateIcon } = require("../middlewares/generateIcon");

// Router
const router = express.Router();

// Make some routes
router.get("/", getMultipleTalents);
router.get("/:id", detailValidator, getDetailTalent);
router.post("/", createValidator, generateIcon, createTalent);
router.put("/:id", detailValidator, updateValidator, updateTalent);
router.delete("/:id", detailValidator, deleteTalent);

// Exports
module.exports = router;
