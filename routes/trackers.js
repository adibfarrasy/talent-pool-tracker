const express = require("express");

// Import validator
const {
  detailValidator,
  createValidator,
  updateValidator,
} = require("../middlewares/validators/trackers");

// Import controller
const {
  getMultipleTrackers,
  getDetailTracker,
  createTracker,
  updateTracker,
} = require("../controllers/trackers");

// Router
const router = express.Router();

// Make some routes
router.get("/", getMultipleTrackers);
router.get("/:id", detailValidator, getDetailTracker);
router.post("/", createValidator, createTracker);
router.put("/:id", detailValidator, updateValidator, updateTracker);

// Exports
module.exports = router;
