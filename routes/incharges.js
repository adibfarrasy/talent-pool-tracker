const express = require("express");

// Import validator
const {
  detailValidator,
  createValidator,
  updateValidator,
} = require("../middlewares/validators/incharges");

// Import controller
const {
  getMultipleInCharges,
  getDetailIC,
  createInCharge,
  updateInCharge,
  deleteInCharge,
} = require("../controllers/incharges");

const { generateIcon } = require("../middlewares/generateIcon");

// Router
const router = express.Router();

// Make some routes
router.get("/", getMultipleInCharges);
router.get("/:id", detailValidator, getDetailIC);
router.post("/", createValidator, generateIcon, createInCharge);
router.put("/:id", detailValidator, updateValidator, updateInCharge);
router.delete("/:id", detailValidator, deleteInCharge);

// Exports
module.exports = router;
