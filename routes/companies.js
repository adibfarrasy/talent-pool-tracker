const express = require("express");

// Import validator
const {
  detailValidator,
  createValidator,
  updateValidator,
} = require("../middlewares/validators/companies");

// Import controller
const {
  getMultipleCompanies,
  getDetailCompany,
  createCompany,
  updateCompany,
  deleteCompany,
} = require("../controllers/companies");

const { generateIcon } = require("../middlewares/generateIcon");

// Router
const router = express.Router();

// Make some routes
router.get("/", getMultipleCompanies);
router.get("/:id", detailValidator, getDetailCompany);
router.post("/", createValidator, generateIcon, createCompany);
router.put("/:id", detailValidator, updateValidator, updateCompany);
router.delete("/:id", detailValidator, deleteCompany);

// Exports
module.exports = router;
