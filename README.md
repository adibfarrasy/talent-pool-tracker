# Talent Pool Tracker

## Installation

Go to the directory where you clone this repository and run `npm install`.

## Sandbox Mode

### Setup

- Create a `.env.development` file, and add the MongoDB URI e.g. `mongodb+srv://tester:ICdFrO4wgaUZyCHf@cluster0.smgmk.mongodb.net/talent_pool_dev`
- The app runs in port 3000 by default, but you can specify your own port in the `.env.development` e.g. `PORT=5000`.
- Create a `.env.test` file for testing with the same format as `.env.development`. Use `mongodb+srv://tester:ICdFrO4wgaUZyCHf@cluster0.smgmk.mongodb.net/talent_pool_test`

### Run

- Run the program using `npm run dev`, OR
- Test the program, using `npm run test`

## API Documentation

Check the API Postman documentation here: https://documenter.getpostman.com/view/12533408/UUy664Zn

## Live Webserver

The live server is http://18.141.180.160:3003/
