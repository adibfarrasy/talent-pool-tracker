const validator = require("validator");
const { Company, Talent, InCharge, Tracker } = require("../../models");

exports.detailValidator = async (req, res, next) => {
  try {
    if (!validator.isMongoId(req.params.id)) {
      return next({ message: "ID is not valid", statusCode: 400 });
    }

    const data = await Tracker.findById(req.params.id);

    if (!data) {
      return next({ message: "Tracker not found.", statusCode: 400 });
    }

    next();
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
};

exports.createValidator = async (req, res, next) => {
  try {
    if (!req.body.talent || !validator.isMongoId(req.body.talent)) {
      return next({
        message: "Talent ID is missing/ invalid",
        statusCode: 400,
      });
    }
    if (!req.body.pic || !validator.isMongoId(req.body.pic)) {
      return next({ message: "PIC ID is missing/ invalid", statusCode: 400 });
    }
    if (!req.body.company || !validator.isMongoId(req.body.company)) {
      return next({
        message: "Company ID is missing/ invalid",
        statusCode: 400,
      });
    }

    const talentData = await Talent.findById(req.body.talent);

    if (!talentData) {
      return next({ message: "Talent not found.", statusCode: 400 });
    }

    const inchargeData = await InCharge.findById(req.body.pic);

    if (!inchargeData) {
      return next({ message: "PIC not found.", statusCode: 400 });
    }

    const companyData = await Company.findById(req.body.company);

    if (!companyData) {
      return next({ message: "Company not found.", statusCode: 400 });
    }

    next();
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
};

exports.updateValidator = async (req, res, next) => {
  try {
    if (req.body.talent) {
      if (!validator.isMongoId(req.body.talent)) {
        return next({ message: "Talent ID is not valid", statusCode: 400 });
      }

      const talentData = await Talent.findById(req.body.talent);

      if (!talentData) {
        return next({ message: "Talent not found.", statusCode: 400 });
      }
    }

    if (req.body.pic) {
      if (!validator.isMongoId(req.body.pic)) {
        return next({ message: "PIC ID is not valid", statusCode: 400 });
      }
      const inchargeData = await InCharge.findById(req.body.pic);

      if (!inchargeData) {
        return next({ message: "PIC not found.", statusCode: 400 });
      }
    }

    if (req.body.company) {
      if (!validator.isMongoId(req.body.company)) {
        return next({ message: "Company ID is not valid", statusCode: 400 });
      }

      const companyData = await Company.findById(req.body.company);

      if (!companyData) {
        return next({ message: "Company not found.", statusCode: 400 });
      }
    }

    next();
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
};
