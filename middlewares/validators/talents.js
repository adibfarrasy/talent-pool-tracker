const validator = require("validator");
const { Talent } = require("../../models");
const crypto = require("crypto");
const path = require("path");
const { promisify } = require("util");

exports.detailValidator = async (req, res, next) => {
  try {
    if (!validator.isMongoId(req.params.id)) {
      return next({ message: "ID is not valid", statusCode: 400 });
    }

    const data = await Talent.findById(req.params.id);

    if (!data) {
      return next({ message: "Talent not found.", statusCode: 400 });
    }

    next();
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
};

exports.createValidator = async (req, res, next) => {
  try {
    if (!req.body.name || !req.body.name.length) {
      return next({ message: "Name is required.", statusCode: 400 });
    }

    req.body.type = "talents";
    next();
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
};

exports.updateValidator = async (req, res, next) => {
  try {
    if (req.body.name?.length === 0) {
      return next({ message: "Name is invalid.", statusCode: 400 });
    }

    /* istanbul ignore next */
    if (req.files?.photo) {
      const errorMessages = [];

      const file = req.files.photo;

      // Make sure image is photo
      if (!file.mimetype.startsWith("image")) {
        errorMessages.push("File must be an image");
      }

      // Check file size (max 1MB)
      if (file.size > 1000000) {
        errorMessages.push("Image must be less than 1MB");
      }

      // If error
      if (errorMessages.length > 0) {
        return next({ statusCode: 400, messages: errorMessages });
      }

      // Create custom filename
      let fileName = crypto.randomBytes(16).toString("hex");

      // Rename the file
      file.name = `${fileName}${path.parse(file.name).ext}`;

      // Make file.mv to promise
      const move = promisify(file.mv);

      // Upload image to /public/images
      await move(`./public/images/talents/${file.name}`);

      // assign req.body.image with file.name
      req.body.photo = `/public/images/talents/${file.name}`;
    }

    next();
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
};
