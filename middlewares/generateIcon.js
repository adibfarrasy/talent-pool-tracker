const jdenticon = require("jdenticon");
const fs = require("fs");
const crypto = require("crypto");

exports.generateIcon = (req, res, next) => {
  const size = 200;
  const id = crypto.randomBytes(16).toString("hex");

  req.body.photo = `/public/images/${req.body.type}/${id}.png`;
  const png = jdenticon.toPng(id, size);
  fs.writeFileSync(`.${req.body.photo}`, png);
  next();
};
